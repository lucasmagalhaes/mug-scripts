#!/bin/bash

readonly EPICS_MOUNT="/home/lamm/e3/epics"
readonly E3_WRAPPERS="/home/lamm/e3/wrappers"
readonly E3_SOURCE="/epics/base-7.0.6.1/require/4.0.0/bin/setE3Env.bash"
readonly DOCKER_WRAPPERS="/home/lamm/wrappers"
readonly DOCKER_IMAGE="centos_e3_lamm"

print_message() {
    echo \
" Usage: e3_docker.sh OPTION
  Assistant for running e3 over docker

  options:
  build <target_module> - builds target module
  tag <target_modue> - print tag for the target module
  install_e3 - populates an epics directory
  exec <cmd> - run the cmd inside the docker"
}

check_epics_dir(){
    if [[ ! -d ${EPICS_MOUNT} ]]; then
        echo "EPICS directory don't exists"
        exit 1
    fi
}

build() {
    local target=${1}
    local build_cmd="cd ${DOCKER_WRAPPERS}/$target;\
source ${E3_SOURCE};\
make init patch build install"

    check_epics_dir

    if [[ ! -d ${E3_WRAPPERS}/${target} ]]; then
        echo "Target repository don't exists"
        exit 1
    fi

    docker run --rm \
        --mount type=bind,source=${EPICS_MOUNT},target="/epics" \
        --mount type=bind,source=${E3_WRAPPERS},target="/home/lamm/wrappers" \
        ${DOCKER_IMAGE} \
        bash -c "${build_cmd}"
}

create_tag() {
    local target=${1}
    local cmd="cd ${DOCKER_WRAPPERS}/$target;\
source ${E3_SOURCE};\
e3-tag-local -d ${DOCKER_WRAPPERS}/$target"

    if [[ ! -d ${E3_WRAPPERS}/${target} ]]; then
        echo "Target repository don't exists"
        exit 1
    fi

    check_epics_dir
    docker run --rm \
        --mount type=bind,source=${EPICS_MOUNT},target="/epics" \
        --mount type=bind,source=${E3_WRAPPERS},target="/home/lamm/wrappers" \
        ${DOCKER_IMAGE} \
        bash -c "${cmd}"
}

install_e3() {
    local cmd="pip3 install --user e3 -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple; \
git clone https://gitlab.esss.lu.se/e3/specifications.git; \
e3-build -y -t /epics specifications/specifications/2022q1-core.yml"

    check_epics_dir
    docker run --rm \
        --mount type=bind,source=${EPICS_MOUNT},target="/epics" \
        ${DOCKER_IMAGE} \
        bash -c "${cmd}"
}

docker_exec (){
    local cmd="source ${E3_SOURCE}; ${1}"
    check_epics_dir
    docker run --rm\
        --mount type=bind,source=${EPICS_MOUNT},target="/epics"\
        --mount type=bind,source=${E3_WRAPPERS},target="/home/lamm/wrappers" \
        --mount type=bind,source="/home/lamm/src",target="/home/lamm/src" \
        ${DOCKER_IMAGE} \
        bash -c "${cmd}"
}

case $1 in
    build)
        build "$2"
        ;;
    install_e3)
        install_e3
        ;;
    exec)
        docker_exec "$2"
        ;;
    tag)
        create_tag "$2"
        ;;
    help)
        print_message
        ;;
    *)
        print_message
        ;;
esac
