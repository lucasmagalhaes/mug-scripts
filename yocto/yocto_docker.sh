#!/bin/bash

readonly YOCTO_SOURCE="/home/lamm/src/yocto-ess"
readonly YOCTO_DIR="/home/lamm/src/yocto-ess"
readonly DOCKER_IMAGE="centos_yocto_lamm"

print_message() {
    echo \
" Usage: yocto_docker.sh OPTION
  Assistant for building yocto under centos7 docker

  options:
  bitbake <target_artifact> - builds target artifact
  exec <cmd> - run the cmd inside the docker"
}

check_yocto_source(){
    if [[ ! -d ${YOCTO_SOURCE} ]]; then
        echo "YOCTO directory don't exists"
        exit 1
    fi
}

bitbake() {
    local target=${1}
    local bitbake_cmd="cd ${YOCTO_DIR};\
export LANG=en_US.UTF-8;\
. ./setup-env -m ifc14xx-64b -t 8 -j 8;\
bitbake ${1}"

    check_yocto_source

    docker run --rm \
        --mount type=bind,source="${YOCTO_SOURCE}",target="${YOCTO_DIR}" \
        ${DOCKER_IMAGE} \
        bash -c "${bitbake_cmd}"
}

docker_exec (){
    local cmd="${1}"
    check_yocto_source
    docker run --rm\
        --mount type=bind,source="${YOCTO_SOURCE}",target="${YOCTO_DIR}" \
        ${DOCKER_IMAGE} \
        bash -c "${cmd}"
}

case $1 in
    bitbake)
        bitbake "${*:2}"
        ;;
    exec)
        docker_exec "${*:2}"
        ;;
    help)
        print_message
        ;;
    *)
        print_message
        ;;
esac
